﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CokingPlace.Models;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Drawing;
using System.Web.Security;

namespace CokingPlace.Controllers
{
    public class RecipesController : Controller
    {
        private CookingBookDbContext db = new CookingBookDbContext();

        public ActionResult Index(int categoryId = 0, string search = "")
        {
            List<Recipe> list;

            if (categoryId == 0 && string.IsNullOrEmpty(search))
            {
                return View(db.Recipes.ToList()) ;
            }

            if (!string.IsNullOrEmpty(search))
            {
                list = db.Recipes.Where(x => x.Name.Contains(search)).ToList();
            }
            else
            {
                ViewBag.PageHeader = db.Categories.Single(x => x.Id == categoryId).Name;
                list = db.Recipes.Where(x => x.CategoryId == categoryId).ToList();
            }
            return View(list);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsAuthor = User.Identity.GetUserId() == recipe.UserId;

            return View(recipe);
        }

        [Authorize]
        public ActionResult Create()
        {
            SetCategoriesViewBag();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Recipe recipe, string categories, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {

                recipe.CreationDate = DateTime.Today;
                recipe.UserId = User.Identity.GetUserId();
                recipe.UserName = User.Identity.Name;

                SetPicture(recipe, uploadImage);

                int categoryId;

                if (!int.TryParse(categories, out categoryId))
                {
                    recipe.CategoryId = 1;
                }
                else recipe.CategoryId = categoryId;

                db.Recipes.Add(recipe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(recipe);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Recipe recipe = db.Recipes.Find(id);

            if (recipe == null)
            {
                return HttpNotFound();
            }

            SetCategoriesViewBag();

            return View(recipe);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Recipe recipe, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                if (uploadImage != null)
                    SetPicture(recipe, uploadImage);
                db.Entry(recipe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(recipe);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }
            return View(recipe);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Recipe recipe = db.Recipes.Find(id);
            db.Recipes.Remove(recipe);
            db.SaveChanges();
            DeleteOldPicture(recipe.ImageUrl);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void SetCategoriesViewBag()
        {
            var categories = new List<SelectListItem>();

            foreach (var item in db.Categories)
            {
                categories.Add(new SelectListItem
                { Text = item.Name, Value = item.Id.ToString() });
            }

            ViewBag.categories = categories;
        }

        private void SetPicture(Recipe recipe, HttpPostedFileBase uploadImage)
        {
            if (!string.IsNullOrEmpty(recipe.ImageUrl))
                DeleteOldPicture(recipe.ImageUrl);

            string fileName = "", uploadUrl, serverUrl;
            serverUrl = "~/Content/Images/RecipeImages";
            uploadUrl = Server.MapPath(serverUrl);

            try
            {
                using (var bitmap = new Bitmap(uploadImage.InputStream))
                {
                    fileName = Guid.NewGuid().ToString() + Path.GetExtension(uploadImage.FileName);
                }
            }
            catch
            {
                fileName = "noimage.png";
                return;
            }
            finally
            {
                recipe.ImageUrl = Path.Combine(serverUrl, fileName);
            }

            uploadImage.SaveAs(Path.Combine(uploadUrl, fileName));
        }

        private void DeleteOldPicture(string url)
        {
            url = Server.MapPath(url);

            if ((System.IO.File.Exists(url)))
            {
                System.IO.File.Delete(url);
            }
        }
    }
}
