﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CokingPlace.Models
{
    public class CookingBookDbContext : DbContext
    {
        public CookingBookDbContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new CookingBookDbInitializer());
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Recipe> Recipes { get; set; }
    }
}