﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CokingPlace.Models
{
    public class CookingBookDbInitializer : CreateDatabaseIfNotExists<CookingBookDbContext>
    {
        protected override void Seed(CookingBookDbContext context)
        {
            context.Categories.Add(new Category { Name = "category" });

            base.Seed(context);
        }
    }
}