create database Warehouse
use Warehouse

create table Materials(
	id int identity(1,1) not null primary key,
	name varchar(20),
	materialDescription text
)

create table Providers(
	id int identity(1,1) not null primary key,
	name varchar(20),
	providerDescription text
)

create table FactoryDepartments(
	id int identity(1,1) not null primary key,
	name varchar(20),
	materialDescription text
)

EXEC sp_RENAME 'FactoryDepartments.materialDescription' , 'departmentDescription', 'COLUMN'

create table Contracts(
	id int identity(1,1) not null primary key,
	wareAmount real,
	materialId int constraint material_fk references Materials(id) on update cascade on delete set null,
	providerId int constraint provider_fk references Providers(id) on update cascade on delete set null
)

create table IncomeNotes(
	id int identity(1,1) not null primary key,
	arrivalDate datetime,
	materialAmount real,
	materialId int constraint income_material_fk references Materials(id) on update cascade on delete set null,
	providerId int constraint note_provider_fk references Providers(id) on update cascade on delete set null
)

create table OutcomeNotes(
	id int identity(1,1) not null primary key,
	shipmentDate datetime,
	materialAmount real,
	materialId int constraint outcome_material_fk references Materials(id) on update cascade on delete set null,
	departmentId int constraint department_fk references FactoryDepartments(id) on update cascade on delete set null
)

insert into Materials(name, materialDescription)
values('Silk', 'Nice stuff'),
		('Leather', 'Super nice stuff'),
		('Cotton', 'Just something'),
		('Rubber', 'Necessary stuff')

select * from Materials

insert into Providers(name, providerDescription)
values('Johnson', 'Blala'),
		('CoalaCo', 'Cutie'),
		('MyProv', 'Nice provider')

select * from Providers

insert into Contracts(wareAmount, materialId, providerId)
values(123.2, 1, 1),
		(5555.6, 2, 2),
		(3589, 4, 3)

select * from Contracts

insert into FactoryDepartments(name, departmentDescription)
	values('The First Department', 'The biggest one'),
			('Tiny department', 'Little one')
select * from FactoryDepartments

insert into IncomeNotes(arrivalDate, materialAmount, providerId, materialId)
	values('2017-05-04', 88, 2, 2),
			('2017-28-02', 30, 3, 4)

select * from IncomeNotes

insert into OutcomeNotes(shipmentDate, materialAmount, departmentId, materialId)
	values('2017-05-01', 12, 1, 3),
			('2017-16-03', 31, 2, 5),
			('2017-01-04', 12.3, 1, 4)

select * from OutcomeNotes