create view lab4_query1 as
	select * from IncomeNotes
		WHERE DATEPART(month, arrivalDate) = 3
select * from lab4_query1

create view lab4_query2 as
	select * from Contracts
		where wareAmount > 100
select * from lab4_query2

create view lab4_query3 as 
	select * from Materials
		where name like 'S%'
select * from lab4_query3

create view lab4_query4 as
	select * from OutcomeNotes
		where materialAmount between 10 and 40
select * from lab4_query4

create view lab4_query5 as
	select materialId, name, materialDescription, materialAmount, shipmentDate 
		from OutcomeNotes inner join Materials on OutcomeNotes.materialId = Materials.id
select * from lab4_query5

create view lab4_query6 as
	select materialId, name, SUM(wareAmount) as totalWareAmount
		from Materials inner join Contracts 
		on Materials.id = Contracts.materialId
		group by materialId, name
select * from lab4_query6

create view lab4_query7 as
	select Contracts.providerId, SUM(Contracts.wareAmount) as plannedAmount, SUM(IncomeNotes.materialAmount) as currentAmount,
		SUM(Contracts.wareAmount) - SUM(IncomeNotes.materialAmount) as diff
		from Contracts inner join IncomeNotes on Contracts.providerId = IncomeNotes.providerId
		group by Contracts.providerId
select * from lab4_query7

create view lab4_query8 as
	select top 10 providerId, COUNT(materialId) as providerContractCount
		from Contracts
		group by providerId
		order by providerContractCount desc
select * from lab4_query8

create view lab4_query9 as
	select * from IncomeNotes
		where DATEPART(MONTH, arrivalDate) = MONTH(GETDATE())
			and DATEPART(YEAR, arrivalDate) = YEAR(GETDATE())
select * from lab4_query9

create view lab4_query10 as
	select name, shipmentDate, departmentId, AVG(materialAmount) as avgAmount
		from OutcomeNotes inner join Materials on materialId = Materials.id
		group by name, shipmentDate, departmentId
		having AVG(materialAmount) > 35
select * from lab4_query10