--1
declare @studentLastName varchar(20)='������'
delete 
	from Reiting
	where Kod_student = (select Kod_stud 
							from dbo_student
							where dbo_student.Sname = @studentLastName)
--2
declare @predmetName varchar(30)='������� ������������'
delete 
	from Rozklad_pids
	where Rozklad_pids.K_predm_pl in (select Predmet_plan.K_predm_pl
							from Predmet_plan
							inner join predmet on Predmet_plan.K_predmet = predmet.K_predmet
							where Nazva = '������� ������������')
select * from Rozklad_pids

--3
update Reiting
	set Reiting = Reiting*0.15
	where K_zapis in (select K_zapis
						from Reiting
						inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
						where month(Rozklad_pids.Date) = 12)
select * 
	from Reiting
	where month(Rozklad_pids.date) = 12

--4
update Reiting
	set Reiting = Reiting/0.15

select * 
	from Reiting

--5
declare @group char(3)=373
declare @studentId int, @mark int = 82, @kod int = 19
declare cur cursor for 
	select kod_stud 
		from dbo_student
		where Kod_group = @group

open cur
fetch next from cur into @studentId
while @@FETCH_STATUS = 0
begin
	insert into Reiting(K_zapis, Kod_student, Reiting, Prisutn)
		values(@kod, @studentId, @mark, 1)
	fetch next from cur into @studentId
end

select * from Reiting

--7
declare @predmetId int = 477, @newPlan int = 18
update Predmet_plan
	set K_navch_plan = @newPlan
	where K_predmet = @predmetId

--8
declare @groupNo int = 10
delete from dbo_student
	where Kod_group = @groupNo

--9
declare @groupId char(3) = 373,
		@kod_para int = 29,
		@pris bit = 1

declare @randomStudent int = (select top 1 percent Kod_stud 
			from dbo_student
			where kod_group = @groupId
			order by newid())

insert into Reiting(K_zapis, Kod_student, Prisutn)
	values(@kod_para, @randomStudent, @pris)

--10
declare @studentName varchar(15) = '���������'
update Reiting
	set Prisutn = 1
	where exists(select Kod_student
					from dbo_student 
					where Reiting.Kod_student = dbo_student.Kod_stud 
						and Sname = @studentName)